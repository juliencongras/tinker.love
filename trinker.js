const { includes, sort } = require("./people");
const people = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },
    
    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    //Tableau des personnes
    allMale: function(p){
        return people.filter(i => i.gender == "Male")
    },
    //Retourne juste les hommes

    //Tableau des personnes
    allFemale: function(p){
        return people.filter(i => i.gender == "Female")
    },
    //Retourne juste les femmes

    //Tableau des personnes, la zone "gender" du tableau
    bygenre: function(p, genre){
        return p.filter(i=>i.gender===genre);
    },
    //Retourne juste le sex qu'on à demandé

    //Tableau des personnes
    nbOfMale: function(p){
        return this.allMale(p).length
    },
    //Sort le nombre d'hommes

    //Tableau des personnes
    nbOfFemale: function(p){
        return this.allFemale(p).length
    },
    //Sort le nombre de femmes

    //Tableau des personnes
    nbOfMaleInterest: function(p){
        nbmaleinterest = people.filter(i => i.looking_for == "M");
        return nbmaleinterest.length;
    },
    //Sort le nombre de perssonnes interessé par les hommes

    //Tableau des personnes
    nbOfFemaleInterest: function(p){
        nbfemaleinterest = people.filter(i => i.looking_for == "F");
        return nbfemaleinterest.length;
    },
    //Sort le nombre de personnes interessé par les femmes

    //Les different parametres du tableau
    slice: function(i){
        return parseFloat(i.income.slice(1))
    },
    //Sort les salaires avec le '$' coupé

    //Tableau des personnes
    incomeOver2000: function(p){
        over2000 = p.filter(i => this.slice(i) > 2000);
        return over2000.length;
    },
    //Sort le nombre de personnes qui ont un salaire de plus de 2000$

    //Tableau des personnes
    likeDrama: function(p){
        dramaLength = people.filter(i => i.pref_movie.includes("Drama"));
        return dramaLength.length;
    },
    //Sort le nombre de personnes qui aime les dramas

    //Tableau des personnes
    femaleSF: function(p){
        SFfemale = this.allFemale(p).filter(i => i.pref_movie.includes("Sci-Fi"));
        return SFfemale.length
    },
    //Sort le nombre de femmes qui aime la science fiction

    //Tableau des personnes
    incomeOver1482: function(p){
        over1482 = p.filter(i => this.slice(i) > 1482);
        return over1482;
    },
    //Les personnes qui touche plus de 1482$

    //Tableau des personnes
    docu1482: function(p){
        docuMoreThan1482 = this.incomeOver1482(p).filter(i => i.pref_movie.includes("Documentary"));
        return docuMoreThan1482.length
    },
    //Le nombre de personne qui touche plus de 1482$ et aime les documentaires

    //Tableau des personnes
    listOver4000: function(p){
        r = [];
        for (let i of p){
                let simple = {
                    id : i.id, nom: i.first_name +" "+ i.last_name, income: i.income
                }
            r.push(simple)}
        return r.filter(i => this.slice(i) > 4000);
    },
    //Nom, prénom, id et revenus des personnes touchant + de 4000$
    
    /*fullList = p.filter(i => this.slice(i) > 4000); Pour avoir tout ceux qui touche + de 4000$*/
    
    //Tableau des personnes
    richestMan: function(p) {
        /*utilise sort => R = [plus pauvre ... plus riche] aka exemple P.sort ((a, b)=>a Fn - b Fn)
        return R[R.length -1]
        */
        /*people.slice;
        people.sort((a, b)=>a Fn - b Fn);*/
        r = []

        for (let i of p){
            let simple = {
                gender : i.gender, 
                income : parseFloat(i.income.slice(1)), 
                id : i.id, name : i.first_name +" "+ i.last_name
            };
            r.push(simple)
        }

        r = r.filter( 
            function(i){
                return i.gender == "Male"
            }
         );

        r.sort(function(a, b) {
            return a.income - b.income;
        });

        return r[r.length - 1]
        //enlever les données inutiles
    },
    //Sort l'homme le plus riche (sex, revenu, id, nom, prénom)

    //Tableau des personnes
    averageSalary: function(P){
        /*P = [tableau]
        acc = ??? (0?)
        for (let I of P){
        acc = acc + I.income}
        Moyenne = Acc/P.length
        */
        acc = 0

        for (let I of P){
            income = parseFloat(I.income.slice(1)), 
            acc = acc + income
        }

        Moyenne = acc/P.length

        return Moyenne;
    },
    //Moyenne des salaires
    
    //Tableau des personnes
    sortIncome: function(p){
        r = []

        for (let i of p){
            let simple = {
                income : parseFloat(i.income.slice(1))
            };
            r.push(simple)
        }

        r.sort(function(a, b) {
            return a.income - b.income;
        });
    },
    //Salaires des personnes dans l'ordre

    //Tableau des personnes
    medianSalary: function(p){
        /*sort le tableau(income)
        Pour calculer une moyenne:
        Si impaire: (length-1)/2
        Si pair: si tableau s'appelle T,
        (T[T.length/2] + T[(T.length/2) - 1])/2
        */
        r = []

        for (let i of p){
            r.push(parseFloat(i.income.slice(1)))
        }

        r.sort(function(a, b) {
            return a - b;
        });
        median = (r[r.length/2] + r[(r.length/2) - 1])/2;
        return median;
    },
    //Le salaire médian

    //Tableau des personnes
    peopleNorthHemisphere: function(p){
        /*avoir les gens qui n'ont que latitude > 0
        return tableau.length*/
        tab = people.filter(i => i.latitude > 0)
        return tab.length
    },
    //Le nombre de personnes dans l'hemisphere nord

    //Tableau des personnes
    averageSalarySouthHemisphere: function(p){
        /*avoir les gens qui ont latitude < 0
        et calculer leur salaire average*/
        south = people.filter(i => i.latitude < 0)
        average = this.averageSalary(south)
        return average;
    },
    //La moyenne des salaires des personnes dans l'hémisphère sud

    //Tableau des personnes
    closerToBereniceCawt: function(p){
        //plus proche de bérenice cawt nom et id
        //Utiliser Pythagore
        
        r = []

        //pythagore pour avoir la distance entre deux points
        for (let i of p){
            
            p1 = /*Les coordonnées de Cawt (Les mettre en dynamique)*/{x:-87.879523 , y:15.5900396}/*x:-87.879523 , y:15.5900396*/
            p2 = /*Les coordonnées de tout le monde*/{x:i.longitude , y:i.latitude}

            a = p2.y - p1.y
            b = p2.x - p1.x
            c = Math.sqrt ((a*a)+(b*b))
            
            let simple = { 
                latitude : i.latitude, longitude : i.longitude, 
                id : i.id, name : i.first_name +" "+ i.last_name,
                distance : c
            };
            r.push(simple)
        }

        r.sort(function(a, b) {
            return a.distance - b.distance;
        })

        return r[1]
    },
    //Personne la plus proche de Bérénice Cawt (coordonnées, id, nom, prénom et distance)

    //Tableau des personnes
    closerToRuiBrach: function(p){
        
        r = []

        for (let i of p){
            
            p1 = {x:120.16366 , y:33.347316}
            p2 = {x:i.longitude , y:i.latitude}

            a = p2.y - p1.y
            b = p2.x - p1.x
            c = Math.sqrt ((a*a)+(b*b))
            
            let simple = { 
                latitude : i.latitude, longitude : i.longitude, 
                id : i.id, name : i.first_name +" "+ i.last_name,
                distance : c
            };
            r.push(simple)
        }

        r.sort(function(a, b) {
            return a.distance - b.distance;
        })

        return r[1]
    },
    //Personne la plus proche de Rui Brach (coordonnées, id, nom, prénom et distance)

    //Tableau des personnes
    tenClosestToJoseeBoshard: function(p){
        
        r = []

        for (let i of p){
            
            p1 = {x:11.9704401 , y:57.6938555}
            p2 = {x:i.longitude , y:i.latitude}

            a = p2.y - p1.y
            b = p2.x - p1.x
            c = Math.sqrt ((a*a)+(b*b))
            
            let simple = { 
                latitude : i.latitude, longitude : i.longitude, 
                id : i.id, name : i.first_name +" "+ i.last_name,
                distance : c
            };
            r.push(simple)
        }

        r.sort(function(a, b) {
            return a.distance - b.distance;
        })

        return r.slice(1, 11)
    },
    //Les dix personnes les plus proches de Josee Boshard (coordonnées, id, nom, prénom et distance)

    //Tableau des personnes
    worksAtGoogle: function(p){
        //return people.filter(i => i.email == "google")
        //en gros ça mais il faut un include à la place
        r = []
        
        for (let i of p){
            let simple = { 
                id : i.id, name : i.first_name +" "+ i.last_name,
                email : i.email
            };
            r.push(simple)}

        return r.filter(i => i.email.includes("google"))
    },
    //Les personnes qui travaillent à google (id, nom, prénom et email)
    
    //fonction choppée sur google pour calculer l'age en fonction de l'année de naissance
    //La date de naissance que l'on veut changé en age
    getAge(dateString) {
        var ageInMilliseconds = new Date() - new Date(dateString);
        return Math.floor(ageInMilliseconds/1000/60/60/24/365); // convert to years
    },
    //Age par rapport à la date de naissance
    
    //Tableau des personnes
    tableauAge: function(p){
        r = []

        for (let i of p){

            let simple = {
                Age : this.getAge(i.date_of_birth),  
                id : i.id, name : i.first_name +" "+ i.last_name
            };

            r.push(simple)
        };
    },
    //Tableau des ages des personnes (Age, id, nom et prénom)

    //Tableau des personnes
    theOldest: function(p){
        //Nom et id
        //Trouver comment mettre la dob en chiffres
        //Ne pas utiliser splice() ça change la database originel
        this.tableauAge(p)

        r.sort(function(a, b) {
            return a.Age - b.Age;
        });

        return r[r.length - 1]
    },
    //Personne la plus vieille (Age, id, nom et prénom)

    //Tableau des personnes
    theYoungest: function(p){
        //Nom et id
        this.tableauAge(p)

        r.sort(function(a, b) {
            return b.Age - a.Age;
        });

        return r[r.length - 1]
    },
    //Personne la plus jeune (Age, id, nom et prénom)

    //Tableau des personnes
    averageAgeDifference: function(p){
        /*r = []

        acc = 0

        for (let i of p){
            let simple = {
                Age : this.getAge(i.date_of_birth),  
            };
            r.push(simple)

            /*Prendre l'indice [0]
            Le comparer au reste du tableau moins lui même
            Enlever l'indice [0] du tableau
            Prendre l'indice [1]
            Le comparer au reste du tableau moins lui même
            Enlever l'indice [1] du tableau
            Etc...
            Ceci sera 'agedifferencetotal'*/

            /*acc = acc + agedifferencetotal
        }

        Moyenne = acc/p.length

        return Moyenne;*/

    },

    mostPopularMovieGenre: function(p){

    },

    moviesGenrePopularityOrder: function(p){

    },
    
    match: function(p){
        return "not implemented".red;
    }
}